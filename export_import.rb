# Export

SERVER = <your_host_name>
KEY_FILE = "/root/.ssh/id_rsa"
SERVER_USERNAME = <server_username>

user = User.first
projects = Project.all
projects.each do |p|
    p.add_export_job(current_user: user)
 end

exec("ssh #{SERVER_USERNAME}@#{SERVER} mkdir -p /tmp/exports && scp -pr /opt/gitlab/embedded/service/gitlab-rails/public/uploads/-/system/import_export_upload/export_file/* #{SERVER_USERNAME}@#{SERVER}:/tmp/exports")


# Import

path_string = ''
user = User.first
file_paths = Dir.glob('/tmp/exports/*').select { |fn| File.directory?(fn) }


file_paths.each do |path|
    path_string = Dir.glob(path + '/*')[0]
    project_params = {
        :name => path_string.split('/')[4], # have to still workout names
        :path => '',
        :namespace_id => user.namespace_id,
        :file => File.open(path_string)
    }
    @project = ::Projects::GitlabProjectsImportService.new(user, project_params).execute
    puts @project.name
end

    

